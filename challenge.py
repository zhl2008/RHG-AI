#!/usr/bin/env python
# -*- coding: utf-8 -*-


'''
@name challenge module for RHG-AI
@author haozigege@Lancet
@version v0.1
@time 2018.9

This module is used to interact with the nterface, and download/provide the 
challenge information for the fuzzing module

'''

import os
import sys
import config
import json
import requests
import Queue
from random import randint
import threading
import time
import stack
import fmt
from my_file import my_file
from pwn import *
import log


reload(sys)  
sys.setdefaultencoding('utf8')

class challenge():

	def __init__(self):
		self.user = config.user
		self.pwd = config.pwd
		self.interface = config.interface
		self.status_url = config.status_url
		self.flag_url = config.flag_url
		self.headers = {"User-Agent":"haozigege"}
		self.down_path = config.down_path
		self.exploit_queue = Queue.Queue(1000)
		self.download_queue = Queue.Queue(1000)
		self.thread_array = []
		self.thread_watch_span = config.thread_watch_span
		self.challenge = {}

		self.shellcode = []
		self.simple_stack = []
		self.fmt = []
		self.heap = []
		self.unknown = []

		# tell the queue thread the init process has been done
		self.init_fin = 0
		self.get_status()
		self.update_running()



	def update_running(self):
		'''
			To change the status of running to not_start_yet
		'''
		for challenge in self.all_challenges:

			challenge_id = challenge['challengeID']
			info_path = '%s/%s/info' % (self.down_path,str(challenge_id))
			if not os.path.exists(info_path):
				continue
			status = json.loads(open(info_path).read())
			if status['status'] == 'running':
				status['status'] == 'not_start_yet'
				open(info_path,'w').write(json.dumps(status))
				log.warning('changing status to not_start_yet in %s' % str(challenge_id))



	def gen_queue(self):
		'''
			some tricks to determine which challenge should be handled initially
		'''

		#some handle function 
		for challenge in self.all_challenges:
			self.queue.put(challenge['challengeID'])


	def challenge_type(self,challenge_id):
		'''
			to judge whether a challenge is simple stack
		'''

		challenge_path = '%s/%s/bin%s' % (self.down_path,str(challenge_id),str(challenge_id))

		if self.judge_simple_stack(challenge_path):
			#write here to judge a simple stack
			log.info('bin%s is simple_stack'%str(challenge_id))
			self.simple_stack.append(challenge_id)
			return "simple_stack"
		elif self.judge_simple_fmt(challenge_path):
			#write here to judge fmt
			log.info('bin%s is simple_fmt'%str(challenge_id))
			self.fmt.append(challenge_id)
			return "simple_fmt"
		elif self.judge_shell_code_manager(challenge_path):
			#write here to judge shellcode
			log.info('bin%s is shell_code_manager'%str(challenge_id))
			self.shellcode.append(challenge_id)
			return "shell_code_manager"
		elif self.judge_heap_problem(challenge_path):
			#write here to judge heap
			log.info('bin%s is heap_problem'%str(challenge_id))
			self.heap.append(challenge_id)
			return "heap_problem"
		else:
			log.info('bin%s is unknown'%str(challenge_id))
			self.unknown.append(challenge_id)
			return "unknown"
		# random for test
		return randint(0,1)



	def judge_simple_stack(self,challenge_path):
		return False

	def judge_simple_fmt(self,challenge_path):
		io = process(challenge_path)
		io.sendline("%p%p")
		info = io.recvall(timeout = 2)
		time.sleep(0.1)
		io.close()
		if info.find("0x") != -1 or info.find("printf") != -1:
			return True
		return False

	def judge_shell_code_manager(self,challenge_path):
		io = process(challenge_path)
		info = io.recvall(timeout = 2)
		if "shellcode" in info.lower() and "run" in info.lower():
			io.close()
			return True
		io.close()
		return False

	def judge_heap_problem(self,challenge_path):
		io = process(challenge_path)
		judge_token = ["chunk","size","length","add","delete","view","change","edit","create","show","content","exit"]
		info = io.recvall(timeout = 2)
		for token in judge_token:
			if token in info.lower() and info.find("1") != -1:
				io.close()
				return True
		io.close()
		return False



	def challenge_status(self,challenge_id):
		'''
			get status of a specific challenge
		'''
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'

		if os.path.exists(binary_dir) and os.path.exists(binary_log):

			log = json.loads(open(binary_log).read())
			return log['status']

		else:
			return 'not_start_yet'


	def get_status(self):
		'''
			get status(challenges and score) from the interface
		'''

		real_url = self.interface + self.status_url
		
		test = 1
		# loop until there is no error

		while test:
			try:
				r = requests.get(real_url,auth=(self.user,self.pwd),headers=self.headers,verify=False)
				#print r.content
				self.status = json.loads(r.content)
				if not self.status.has_key('AiChallenge'):
					time.sleep(2)
					log.warning('game not start!')
					continue
				test = 0
			except Exception,e:
				test = 1
				log.error(str(e))
		#print r.content
		

		# prepare the download path
		if not os.path.exists(self.down_path):
			os.system('mkdir -p %s' %self.down_path)

		self.all_challenges = self.status['AiChallenge']

		# put all the challenge to download queue
		for challenge in self.all_challenges:
			self.download_queue.put(challenge)
			# print challenge

			tmp = {}
			tmp['path'] = self.down_path + '/' + str(challenge['challengeID']) + '/bin' + str(challenge['challengeID'])
			tmp['file_id'] = challenge['challengeID']
			tmp['flag_path'] = challenge['flag_path']
			tmp['server_ip'] = challenge['vm_ip']
			tmp['server_port'] = challenge['question_port']

			self.challenge[challenge['challengeID']] = tmp




	def get_score(self):
		points = self.status['PointsInfo']['points']
		aiPoints = self.status['PointsInfo']['aiPoints']

		return points,aiPoints



	def download_challenge(self):

		'''
			download the challenges

		'''

		while not self.download_queue.empty():

			challenge = self.download_queue.get()

			challengeID = challenge['challengeID']
			binaryUrl = challenge['binaryUrl']

			os.system('mkdir -p %s/%s' %(self.down_path,challengeID))
			os.system('wget %s --no-check-certificate -O %s/%s/bin%s 1>/dev/null 2>&1' %(binaryUrl,self.down_path,challengeID,challengeID))
			log.success('binary downloaded: %s' %challengeID)

			# add the execute priv
			os.system('chmod +x %s/%s/bin%s'%(self.down_path,challengeID,challengeID))

			# if a challenge is a simple stack challenge or shellcode, do it firstly
			status = self.challenge_status(challengeID)
			c_type = self.challenge_type(challengeID)

			if status == 'abandon' or status == 'done':
				continue

			if c_type == 'simple_stack' and c_type == 'shell_code_manager':
				log.success('add %s into exploit_queue' %str(challengeID))
				self.exploit_queue.put(challengeID)


		self.init_fin = 1

	def check_status(self,challenge_id):
		'''
			judge a status of a challenge
		'''	
		status = self.challenge_status(challenge_id)
		#log.success(str(challenge_id)+" is "+status)
		if status == 'abandon' or status == 'done' or status == 'running':
			return False
		return True


	def flag_submit(self,flag):
		
		log.info("get flag! =>> " + flag)
		real_url = self.interface + self.flag_url
		data = {"answer":flag}

		try:
			r = requests.post(real_url,auth=(self.user,self.pwd),data=data,headers=self.headers,verify=False)
		except Exception,e:
			log.error('error: ' + str(e))
			return 

		self.result = json.loads(r.content)
		#print r.content
		if self.result['status'] == 1:
			log.success('flag correct')
		else:
			 log.error('flag incorrect')
			 log.error(self.result['msg'])

	def queue_thread(self):
		'''
			manage the exploit_queue
		'''
		while self.init_fin != 1:
			time.sleep(1)
		log.info('init finish detected!')

		while True:
			if self.exploit_queue.empty():

				log.warning('empty queue detected!')

				for c in self.shellcode:
					if self.check_status(c):
						self.exploit_queue.put(c)

				for c in self.simple_stack:
					if self.check_status(c):
						self.exploit_queue.put(c)

				for c in self.unknown:
					if self.check_status(c):
						self.exploit_queue.put(c)

				for c in self.fmt:
					if self.check_status(c):
						self.exploit_queue.put(c)

				for c in self.heap:
					if self.check_status(c):
						self.exploit_queue.put(c)

			time.sleep(1)

	def thread_watch(self):
		'''
			start a thread to print the status of all alive status
		'''
		while True:

			time.sleep(self.thread_watch_span)

			res = ''
			res += '######  thread status ######\n'
			for my_thread in self.thread_array:
				if my_thread.isAlive():
					res +=  my_thread.name + ' => ' + 'Alive\n' if my_thread.isAlive() else 'Dead\n'
			res += '######  status ends  ######\n'

			if not self.exploit_queue.empty():
				# print self.exploit_queue.qsize()
				all_exp_challenges = str(list(self.exploit_queue.queue))
			else:
				all_exp_challenges = 'empty'
			res += '[+] existing exp queue: %s'%all_exp_challenges

			log.context(res)


	def exploit_call(self):

		while True:

			# if the exploit queue is empty, wait for 3 seconds
			if self.exploit_queue.empty():
				time.sleep(3)
				continue

			else:
				challenge_id = self.exploit_queue.get()
				log.info('start exploit %s' %str(challenge_id))
				self.attack(challenge_id)
				log.info('exploit for %s done' %str(challenge_id))



	def get_challenge_status(self,challenge_id):
		'''
			get the status of a challenge
		'''
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'

		return json.loads(open(binary_log,'r').read())

	def attack_stack(self, challenge_id):

		status = self.get_challenge_status(challenge_id)
		if status['flag']:
			return status
		if status['stack_mode'] == 'stage0':
			log.success(str(challenge_id)+"=====>>>>stage0")
			status = stack.stack_api(self.challenge[challenge_id],0, 0x80, 1800)
		elif status['stack_mode'] == 'stage1':
			log.success(str(challenge_id)+"=====>>>>stage1")
			status = stack.stack_api(self.challenge[challenge_id],0, 0x290, 1800)
		elif status['stack_mode'] == 'stage2':
			log.success(str(challenge_id)+"=====>>>>stage2")
			status = fmt.fmt_api(self.challenge[challenge_id],0, 0x40, 1800)
		elif status['stack_mode'] == 'stage3':
			log.success(str(challenge_id)+"=====>>>>stage3")
                        status = stack.stack_api(self.challenge[challenge_id],2, 0x80, 1800)
		elif status['stack_mode'] == 'stage4':
			log.success(str(challenge_id)+"=====>>>>stage4")
			status = fmt.fmt_api(self.challenge[challenge_id],2, 0x40, 300)
		elif status['stack_mode'] == 'stage5':
			log.success(str(challenge_id)+"=====>>>>stage5")
			status = stack.stack_api(self.challenge[challenge_id],2, 0x290, 21600)
		elif status['stack_mode'] == 'stage6':
			status['status'] = "abandon"
		return status


	def attack(self,challenge_id):
		'''
			this is an example of what the attack method should be.

			the info file for a challenge:
			id      1
			status  not_start_yet/done/abandon/suspend/running
			flag    flag{81b9c5e90ba6f16fc8a674337fb91623}
			start_time    1537204727
			end_time    1537204737

		'''

		# prepare
		binary_dir = '%s/%s/' % (self.down_path,str(challenge_id))
		binary_log = binary_dir + 'info'
		if os.path.exists(binary_log):
			status = self.get_challenge_status(challenge_id)
			status['status'] = 'running'
			open(binary_log,'w').write(json.dumps(status))
		else:
			status = {}
			status['id'] = challenge_id
			status['status'] = 'running'
			status['flag'] = ''
			status['start_time'] = str(int(time.time()))
			status['end_time'] = ''
			status['stack_mode'] = 'stage0'
			open(binary_log,'w').write(json.dumps(status))

		log.success(str(challenge_id)+"started")

		# exploit and get flag

		# ..........................

		#flag = 'flag{81b9c5e90ba6f16fc8a674337fb91623}'

		status = self.attack_stack(challenge_id)
		if status['flag']:
			flag = status['flag']
			self.flag_submit(flag)
			log.success("Get flag!!!=++>>>>>"+str(challenge_id)+" "+flag)
		# submit flag and done
		#self.flag_submit(flag)
		status['end_time'] = str(int(time.time()))
		#status['flag'] = flag
		#status['status'] = 'done'
		open(binary_log,'w').write(json.dumps(status))



if __name__ == '__main__':
	c = challenge()

	for i in range(config.down_thread):
		t = threading.Thread(target=c.download_challenge,name='download_thread_%d'%(i+1))	
		log.info('one thread for downloading has been created!')
		c.thread_array.append(t)

	for i in range(config.exploit_thread):
		t = threading.Thread(target=c.exploit_call,name='exploit_thread_%d'%(i+1))	
		log.info('one thread for exploiting has been created!')
		c.thread_array.append(t)


	t = threading.Thread(target=c.queue_thread,name='queue_thread')
	c.thread_array.append(t)

	t = threading.Thread(target=c.thread_watch,name='watch_dog')
	c.thread_array.append(t)


	for t in c.thread_array:
		t.setDaemon(True)
		t.start()

	try:
		while True:
			pass
	except KeyboardInterrupt:
		log.error("Program stoped by user, existing...")
		sys.exit()

