import stack
import time

status = {}
status['id'] = "16"
status['status'] = 'running'
status['flag'] = ''
status['start_time'] = str(int(time.time()))
status['end_time'] = ''
status['stack_mode'] = 'stage0'

challenge = {}
challenge['path'] = "/home/angr/RHG-AI/tmp/7/bin7"
challenge['file_id'] = "04"
challenge['flag_path'] = "/tmp/cncert/flag08"
challenge['server_ip'] = "127.0.0.1"
challenge['server_port'] = 9966

print stack.stack_api(challenge,0, 0x100, 30)
