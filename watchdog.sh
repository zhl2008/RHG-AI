#!/bin/bash
now=`date '+%Y-%m-%d %H:%M:%S'`

grepFlag='python challenge.py'
thisLog='/root/watch.log'

baseDir="/root"
sleepTime=10

if [ ! -f "$baseDir/challenge.py" ]; then
    echo "$baseDir/challenge.py missing, check again" > "$thisLog"
    exit
fi


while [ 0 -lt 1 ]
do
    now=`date '+%Y-%m-%d %H:%M:%S'`
    ret=`ps aux | grep "$grepFlag" | grep -v grep | wc -l`
    if [ $ret -eq 0 ]; then
        cd $baseDir
        echo "$now process not exists ,restart process now... " > "$thisLog"
        python challenge.py
        echo "$now restart done ..... "  > "$thisLog"
        cd $curDir
    else
        echo "$now process exists , sleep $sleepTime seconds " > "$thisLog"
    fi
    sleep $sleepTime
done
