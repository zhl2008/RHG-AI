#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import Queue
import subprocess
import threading
from config import *

#core_num = 2
#shellphuzz_path = "/home/angr/.virtualenvs/angr/bin/shellphuzz"
#fuzzdir_path = "/home/angr/fuzzdir"
#extral_cmd = " -c "+str(core_num) #-t 1800 "

def monitor(dst_dir, filename, result_q,timeout):
    found = False
    start_time = time.time()
    while True:
        tmp_time = time.time()
        if tmp_time > start_time+timeout:
            break
        if os.path.exists(dst_dir):
            break
        else:
            # print "[*] %s not exist" % dst_dir
            time.sleep(5)
    count = 0
    while True:
        tmp_time = time.time()
        if tmp_time > start_time+timeout:
            crash_data = ""
            break
        tmp_dir=dst_dir
        if count>0:
            tmp_dir=dst_dir.replace("fuzzer-master","fuzzer-"+str(count))
        count += 1
        count %= core_num
        if not os.path.exists(tmp_dir):
            continue
        for each in os.listdir(tmp_dir):
            #print each
            if each != "README.txt" and os.path.exists(os.path.join(tmp_dir, each)):
                found = True
                with open(os.path.join(tmp_dir, each), "rb") as fp:
                    crash_data = fp.read()
                break

        if found:
            break
        time.sleep(5)
    result_q.put(crash_data)


def startFuzz(target_path,timeout):
    cmd = shellphuzz_path + " -C -w " + fuzzdir_path +  extral_cmd +" -t " + str(timeout) +  " %s > /dev/null 2> /dev/null" % (target_path)
    p = subprocess.Popen(cmd, stdin = subprocess.PIPE, shell = True)
    return p


def startCheckThread(target_path, result_q,timeout):
    dst_dir = os.path.join(fuzzdir_path, os.path.basename(target_path))
    dst_dir = os.path.join(dst_dir, "sync/fuzzer-master/crashes")
    t = threading.Thread(target = monitor, args = (dst_dir, os.path.basename(target_path), result_q,timeout))
    t.start()


def fuzzAndCheck(target_path, timeout):
    t = time.time()
    crash_result_q = Queue.Queue()
    fuzzer = startFuzz(target_path,timeout)
    startCheckThread(target_path, crash_result_q,timeout)

    while crash_result_q.empty():
        # print "[*] Check result..."
        tmp_time = time.time()
	if tmp_time > (t+timeout):
	    os.system("kill -9 %d" % fuzzer.pid)
	    return ""
	time.sleep(5)

    result = crash_result_q.get()
    # print "[+] Find crash, copy it to %s" % result
    os.system("kill -9 %d" % fuzzer.pid)
    return result

def fuzzit(path, timeout = default_timeout):
    if not os.path.exists(fuzzdir_path):
        os.makedirs(fuzzdir_path)
    if not os.path.exists(path):
        print "[-] fuzz-stage file not exist!"
        return ""
    return fuzzAndCheck(path, timeout)


if __name__ == "__main__":
    print fuzzit("/home/angr/RHG_AI/tmp/1/bin1",1800).encode('hex')
