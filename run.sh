#!/bin/bash


# start ssh
sudo service ssh start

# webservice
cd /root/; sudo python server.py  1>/dev/null 2>&1  &

# clear path
mv tmp tmp_old
mv /home/angr/fuzzdir /home/angr/fuzzdir_old


# webdog
sudo /bin/bash -c "source /home/angr/.virtualenvs/angr/bin/activate;/root/watchdog.sh"

sudo /bin/bash
