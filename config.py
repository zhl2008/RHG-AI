#!/usr/bin/env python

'''
	configuration for the game

'''

# username for the challenge interface
#user = 'user13'
user = 'student07'

# password for the challenge interface
#pwd = '524103'
pwd = 'gKSdNx6G'

# url for the interface
#interface = 'https://rhg.ichunqiu.com/rhg'
interface = 'https://172.16.4.1'

# interface for getting the status
status_url = '/api/get_question_status'

# interface for submitting the flag
flag_url = '/api/sub_answer'

# interface for resetting
#reset_url = 'the/api/reset_question'
reset_url = '/api/reset_question'

# interface for getting the machine status
machine_url = '/api/get_machines_info'

# binary download path
down_path = './tmp'

# log file
log_file = 'my_log'

# whether write logs to file
log_to_file = 1

# download challenge thread num
down_thread = 5

# exploit thread num
exploit_thread = 15

# thread watch time span
thread_watch_span = 30

# stack
ROPgadget_path = "/home/angr/.virtualenvs/angr/bin/ROPgadget"
#flag_path = "/home/angr/cncert/flag"
#binary_path = "/home/angr/cncert/bin"

# fuzz
core_num = 2
shellphuzz_path = "/home/angr/.virtualenvs/angr/bin/shellphuzz"
fuzzdir_path = "/home/angr/fuzzdir"
extral_cmd = " -c "+str(core_num)
default_timeout = 1800




